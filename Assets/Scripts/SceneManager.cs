using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneManager : MonoBehaviour
{
    [Header("Finite state machine")]
    public PlayerControl player;
    public RifleControl submachine;
    private void Start()
    {
        //Cursor.lockState = CursorLockMode.Locked;
    }
    private void Update()
    {
        player.stateMachine.Check();
        submachine.stateMachine.Check();
    }
}
