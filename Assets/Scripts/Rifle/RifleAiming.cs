using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RifleAiming : IState
{
    private RifleControl rifle;
    private Transform mainCamera;
    private Transform shoulder;
    private Vector3 shoulderOffset;

    public RifleAiming(RifleControl rifle, Transform mainCamera, Transform shoulder, Vector3 shoulderOffset)
    {
        this.rifle = rifle;
        this.mainCamera = mainCamera;
        this.shoulder = shoulder;
        this.shoulderOffset = shoulderOffset;
    }
    public void OnEnter() { }
    public void OnLoop()
    {
        Physics.Raycast(mainCamera.position, mainCamera.forward, out RaycastHit hit);
        hit.point = hit.point != Vector3.zero ? hit.point : mainCamera.position + mainCamera.forward * 99;
        Debug.DrawLine(rifle.transform.position, hit.point, Color.red);
        Debug.DrawLine(mainCamera.position, hit.point, Color.yellow);
        rifle.transform.position = Vector3.Lerp(rifle.transform.position, shoulder.position + shoulderOffset + mainCamera.forward * 0.7f, 0.5f);
        rifle.transform.forward = Vector3.Lerp(rifle.transform.forward, hit.point - rifle.transform.position, 1f);
    }
    public void OnExit() { }
}
