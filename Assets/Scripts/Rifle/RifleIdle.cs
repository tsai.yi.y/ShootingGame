using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RifleIdle : IState
{
    private RifleControl rifle;
    private Transform mainCamera;

    public RifleIdle(RifleControl rifle, Transform mainCamera)
    {
        this.rifle = rifle;
        this.mainCamera = mainCamera;
    }
    public void OnEnter() { }
    public void OnLoop() { }
    public void OnExit() { }
}
