using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class RifleControl : MonoBehaviour
{
    public Transform shoulder;
    public Vector3 shoulderOffset;

    private Transform mainCamera;
    private Transform player;
    [HideInInspector] public StateMachine stateMachine;
    [HideInInspector] public Vector2 aimingDirection;

    private void Awake()
    {
        mainCamera = Camera.main.transform;
        player = transform.root;
        stateMachine = new StateMachine();
        #region weapon states
        RifleIdle submachineIdle = new RifleIdle(this, mainCamera);
        RifleAiming submachineAiming = new RifleAiming(this, mainCamera, shoulder, shoulderOffset);
        #endregion
        #region weapon state conditions
        void Condition(IState current, IState next, Func<bool> condition) => stateMachine.AddTransition(current, next, condition);
        void AnyCondition(IState next, Func<bool> condition) => stateMachine.AddAnyTransition(next, condition);
        //Condition(submachineIdle, submachineAiming, () => aimingDirection != Vector2.zero);
        //Condition(submachineAiming, submachineIdle, () => aimingDirection == Vector2.zero);
        AnyCondition(submachineAiming, () => aimingDirection != Vector2.zero);
        #endregion
        //weapon start state
        stateMachine.SetState(submachineIdle);
    }
    public void OnCameraRotate(InputAction.CallbackContext context)
    {
        aimingDirection = context.ReadValue<Vector2>();
    }
}
