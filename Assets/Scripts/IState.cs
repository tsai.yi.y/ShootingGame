public interface IState
{
    void OnEnter();
    void OnLoop();
    void OnExit();
}