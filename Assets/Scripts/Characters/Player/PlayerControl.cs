using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
//using static UnityEngine.InputSystem.InputAction;

public class PlayerControl : MonoBehaviour
{
    [HideInInspector] public StateMachine stateMachine;
    [HideInInspector] public Vector2 walkingDirection;
    [HideInInspector] public bool run;

    private Transform mainCamera;
    private Animator animator;

    private void Awake()
    {
        stateMachine = new StateMachine();
        mainCamera = Camera.main.transform;
        animator = GetComponent<Animator>();
        #region player states
        PlayerIdle playerIdle = new PlayerIdle(animator);
        PlayerWalk playerWalk = new PlayerWalk(this, animator, mainCamera);
        //PlayerRun playerRun = new PlayerRun(this, /*animator,*/ mainCamera);
        #endregion
        #region player state conditions
        void Condition(IState current, IState next, Func<bool> condition) => stateMachine.AddTransition(current, next, condition);
        Condition(playerIdle, playerWalk, () => walkingDirection != Vector2.zero/* && !run*/);
        //Condition(playerIdle, playerRun, () => walkingDirection != Vector2.zero && run);
        Condition(playerWalk, playerIdle, () => walkingDirection == Vector2.zero);
        //Condition(playerWalk, playerRun, () => walkingDirection != Vector2.zero && run);
        //Condition(playerRun, playerIdle, () => walkingDirection == Vector2.zero);
        //Condition(playerRun, playerWalk, () => walkingDirection != Vector2.zero && !run);
        #endregion
        //player start state
        stateMachine.SetState(playerIdle);
    }
    //private void Update()
    //{
    //    if (runAction.triggered)
    //    {
    //        Debug.Log("run");
    //    }
    //}
    //private void OnEnable()
    //{
    //    runAction.Enable();
    //}
    //private void OnDisable()
    //{
    //    runAction.Disable();
    //}
    public void OnPlayerWalk(InputAction.CallbackContext context)
    {
        walkingDirection = context.ReadValue<Vector2>();
    }
    public void OnPlayerRun(InputAction.CallbackContext context)
    {
        run = context.ReadValueAsButton();
    }
}
