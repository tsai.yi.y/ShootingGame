using System.Collections;
using System.Collections.Generic;
using UnityEngine;

internal class PlayerRun : IState
{
    private PlayerControl player;
    private Animator animator;
    private Transform mainCamera;
    private int animatorForwardHash;
    private int animatorTurnHash;
    private Vector3 currentForward;

    public PlayerRun(PlayerControl player, Animator animator, Transform mainCamera)
    {
        this.player = player;
        this.animator = animator;
        this.mainCamera = mainCamera;
        animatorForwardHash = Animator.StringToHash("Forward");
        animatorTurnHash = Animator.StringToHash("Turn");
    }
    public void OnEnter()
    {
    }
    public void OnLoop()
    {
        currentForward = Vector3.Cross(mainCamera.right, Vector3.up) * player.walkingDirection.y * 2 + mainCamera.right * player.walkingDirection.x * 2;
        player.transform.forward = Vector3.Lerp(player.transform.forward, currentForward, 0.1f);
        animator.SetFloat(animatorForwardHash, Vector3.Dot(player.transform.forward, currentForward));
        animator.SetFloat(animatorTurnHash, Vector3.Dot(player.transform.right, currentForward));
    }
    public void OnExit()
    {
        player.run = false;
    }
}
