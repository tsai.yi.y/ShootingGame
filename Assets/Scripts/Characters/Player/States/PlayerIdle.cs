using System.Collections;
using System.Collections.Generic;
using UnityEngine;

internal class PlayerIdle : IState
{
    private Animator animator;
    private int animatorForwardHash;
    private int animatorTurnHash;
    public PlayerIdle(Animator animator)
    {
        this.animator = animator;
        animatorForwardHash = Animator.StringToHash("Forward");
        animatorTurnHash = Animator.StringToHash("Turn");
    }
    public void OnEnter() { }
    public void OnLoop()
    {
        animator.SetFloat(animatorForwardHash, Mathf.MoveTowards(animator.GetFloat(animatorForwardHash), 0, 0.1f));
        animator.SetFloat(animatorTurnHash, Mathf.MoveTowards(animator.GetFloat(animatorTurnHash), 0, 0.1f));
    }
    public void OnExit() { }
}
