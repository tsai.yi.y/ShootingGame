using System.Collections;
using System.Collections.Generic;
using UnityEngine;

internal class PlayerWalk : IState
{
    private PlayerControl player;
    private Animator animator;
    private Transform mainCamera;
    private bool run;
    private int animatorForwardHash;
    private int animatorTurnHash;
    private Vector3 currentForward;
    public float smoothTime = 0.3f;
    private Vector3 velocity = Vector3.zero;

    public PlayerWalk(PlayerControl player, Animator animator, Transform mainCamera)
    {
        this.player = player;
        this.animator = animator;
        this.mainCamera = mainCamera;
        animatorForwardHash = Animator.StringToHash("Forward");
        animatorTurnHash = Animator.StringToHash("Turn");
    }
    public void OnEnter() { }
    public void OnLoop()
    {
        currentForward = player.run ?
            Vector3.Cross(mainCamera.right, Vector3.up) * player.walkingDirection.y * 2 + mainCamera.right * player.walkingDirection.x * 2 :
            Vector3.Cross(mainCamera.right, Vector3.up) * player.walkingDirection.y + mainCamera.right * player.walkingDirection.x;
        player.transform.forward = Vector3.Lerp(player.transform.forward, currentForward, 0.5f);
        //player.transform.forward = Vector3.SmoothDamp(player.transform.forward, currentForward, ref velocity, smoothTime);
        player.transform.position += player.transform.forward * 0.1f;
        //Debug.Log($"{currentForward}, {player.transform.forward}, {player.transform.position}");
        animator.SetFloat(animatorForwardHash, Vector3.Dot(player.transform.forward, currentForward));
        animator.SetFloat(animatorTurnHash, Vector3.Dot(player.transform.right, currentForward));
    }
    public void OnExit() { }
}
