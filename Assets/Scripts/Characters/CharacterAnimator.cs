using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAnimator : MonoBehaviour
{
    public Transform followingTarget;

    private Animator animator;
    private int animatorForwardHash;
    private int animatorTurnHash;
    private void Awake()
    {
        animator = GetComponent<Animator>();
        animatorForwardHash = Animator.StringToHash("Forward");
        animatorTurnHash = Animator.StringToHash("Turn");
    }

    private void Update()
    {
        Vector3 currentForward = followingTarget.position - transform.position;
        animator.SetFloat(animatorForwardHash, Vector3.Dot(transform.forward, currentForward));
        animator.SetFloat(animatorTurnHash, Vector3.Dot(transform.right, currentForward));
    }
}
