using UnityEngine;

public class CharacterFeetIK : MonoBehaviour
{
    //public LayerMask layerMask;

    private Animator animator;
    private Transform leftFoot;
    private Transform rightFoot;
    private AvatarIKGoal leftFootGoal = AvatarIKGoal.LeftFoot;
    private AvatarIKGoal rightFootGoal = AvatarIKGoal.RightFoot;
    private int leftFootIKWeight;
    private int rightFootIKWeight;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        leftFoot = animator.GetBoneTransform(HumanBodyBones.LeftFoot);
        rightFoot = animator.GetBoneTransform(HumanBodyBones.RightFoot);
        leftFootIKWeight = Animator.StringToHash("LeftFootIKWeight");
        rightFootIKWeight = Animator.StringToHash("RightFootIKWeight");
    }
    private void OnAnimatorIK(int layerMask)
    {
        if (layerMask == 0) //Base Layer
        {
            Physics.Raycast(leftFoot.position + Vector3.up * 0.5f, Vector3.down, out RaycastHit leftFootHit, 1/*, layerMask*/);
            Debug.DrawLine(leftFoot.position, leftFootHit.point);
            Physics.Raycast(rightFoot.position + Vector3.up * 0.5f, Vector3.down, out RaycastHit rightFootHit, 1/*, layerMask*/);
            Debug.DrawLine(rightFoot.position, rightFootHit.point);
            //Debug.Log($"{leftFootHit.point.y}, {rightFootHit.point.y}");
            if (leftFootHit.point != Vector3.zero)
            {
                animator.SetIKPositionWeight(leftFootGoal, animator.GetFloat(leftFootIKWeight));
                animator.SetIKRotationWeight(leftFootGoal, animator.GetFloat(leftFootIKWeight));
                animator.SetIKPosition(leftFootGoal, leftFootHit.point + Vector3.up * 0.12f);
                animator.SetIKRotation(leftFootGoal, Quaternion.LookRotation(Vector3.Cross(leftFootHit.normal, leftFoot.right), leftFootHit.normal));
            }
            if (rightFootHit.point != Vector3.zero)
            {
                animator.SetIKPositionWeight(rightFootGoal, animator.GetFloat(rightFootIKWeight));
                animator.SetIKRotationWeight(rightFootGoal, animator.GetFloat(rightFootIKWeight));
                animator.SetIKPosition(rightFootGoal, rightFootHit.point + Vector3.up * 0.12f);
                animator.SetIKRotation(rightFootGoal, Quaternion.LookRotation(Vector3.Cross(rightFootHit.normal, rightFoot.right), rightFootHit.normal));
            }
            float newY = leftFootHit.point != Vector3.zero && rightFootHit.point != Vector3.zero ?
                (leftFootHit.point.y + rightFootHit.point.y) / 2 + 0.12f :
                leftFootHit.point == Vector3.zero ? rightFootHit.point.y + 0.12f : leftFootHit.point.y + 0.12f;
            transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x, newY, transform.position.z), 0.1f);
        }
    }
}
