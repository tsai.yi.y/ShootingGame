# Shooting Game Project
## ShootingGame scene
### Input System
- Control character and weapon with finite state machine
### Inverse Kinematics
- Control character feet IK with OnAnimatorIK() and animation curve
### Shaders
- Rim light on the character
